# Programma alternanza scuola lavoro

Il programma prevede l'apprendimento di concetti teorici e la successiva messa in pratica attraverso degli esercizi. Lo studente al termine di ogni argomento teorico dovrà elaborare una presentazione da esporre al tutor. Ogni singolo esercizio verrà discusso e rivisto con l'aiuto del tutor o di un membro del team di Cherrychain.
Il programma esposto rappresenta solo una base che sarà rielaborata in relazione alle capacità dello studente.

Lo studente, oltre a seguire il programma di studio, avrà la possibilità di seguire da vicino le dinamiche di un team agile. In maniera tale da poter vedere come un'azienda agile opera quotidianamente.

## Agile
### Teoria
* [Agile Manifesto](https://agilemanifesto.org/iso/it/manifesto.html)
* [SCRUM](https://www.scrumguides.org/docs/scrumguide/v2017/2017-Scrum-Guide-Italian.pdf)

### Esercizio
[elephant-carpaccio](http://kata-log.rocks/elephant-carpaccio)
- creare una board su trello
- aggiungere le storie
- eseguire le storie

## Git

* Creare profilo [gitlab](https://about.gitlab.com/)
* creare progetto di Test
* eseguire un push
* Tutti gli esercizi dovranno essere versionati su gitlab

## Clean Code
Lettura di alcuni capitoli del libro Clean Code di Robert Cecil Martin. Utili ad affrontare i sucessivi esercizi.

## TDD

### Studio

* Capitolo 9 "The Three Laws of TDD" di Clean Code
* Read Test Driven Development: By Example

### Esercizio
* String Calculator
* [Sales Kata](https://github.com/xpeppers/sales-taxes-problem) in TDD
* [Tennis Kata implementation ] (https://github.com/codecop/Tennis-Kata-ATD2013)

## Ricerche applicative
* Lo studente potrà essere coinvolto in ricerche utili al team

## Principi solidi

### Studio

* [SOLID Principles](http://butunclebob.com/ArticleS.UncleBob.PrinciplesOfOod)

### Esercizio

* [Racing Car Katas](https://github.com/emilybache/Racing-Car-Katas)

